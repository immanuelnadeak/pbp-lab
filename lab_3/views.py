from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)

    if (form.is_valid and request.method == 'POST'):
        # save the form data to model
        form.save()
        # If the request method is POST then we need to redirect to /lab-3
        return redirect('/lab-3')

    response = {'form': form}
    return render(request, 'lab3_form.html', response)