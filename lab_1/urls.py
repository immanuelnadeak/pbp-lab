from django.urls import path
from .views import friend_list, index

urlpatterns = [
    # Baris kode path('', index, name='index'), yang artinya untuk URL dengan awalan / akan dialihkan ke sebuah views bernama index
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('friends', friend_list, name='friend_list'),
]