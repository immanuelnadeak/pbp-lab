import 'package:flutter/material.dart';

class Daerah {
  final String id;
  final String daerah;

  const Daerah({
    required this.id,
    required this.daerah,
  });
}
