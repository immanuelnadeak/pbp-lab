import 'package:flutter/material.dart';

class RumahSakit {
  final String id;
  final String rs;
  final String alamat;
  final int telepon;

  const RumahSakit({
    required this.id,
    required this.rs,
    required this.alamat,
    required this.telepon,
  });
}
