import 'package:flutter/material.dart';
import './screen/daerah_screen.dart';
import './screen/rs_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(
      String title, IconData icon, void Function() tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Daerah",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: DaerahScreen(),
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              Container(
                height: 120,
                width: double.infinity,
                padding: EdgeInsets.all(20),
                alignment: Alignment.centerLeft,
                color: Theme.of(context).accentColor,
                child: Text(
                  'Temenin Isoman',
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 30,
                      color: Theme.of(context).primaryColor),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              buildListTile('Home', Icons.home, () {
                Navigator.of(context).pushReplacementNamed('/');
              }),
              ExpansionTile(
                title: Text('Isolasi Mandiri'),
                children: [
                  buildListTile('Deteksi Mandiri', Icons.quiz_rounded, () {
                    Navigator.of(context).pushReplacementNamed('/');
                  }),
                  buildListTile('Checklist', Icons.checklist_rounded, () {
                    Navigator.of(context).pushReplacementNamed('/');
                  }),
                  buildListTile('Happy Notes', Icons.note, () {
                    Navigator.of(context).pushReplacementNamed('/');
                  }),
                ],
              ),
              buildListTile('Tips and Tricks', Icons.star_rounded, () {
                Navigator.of(context).pushReplacementNamed('/');
              }),
              buildListTile('Symptoms and Medicines', Icons.medication_rounded,
                  () {
                Navigator.of(context).pushReplacementNamed('/');
              }),
              buildListTile('Info Bed Capacity', Icons.bed_rounded, () {
                Navigator.of(context).pushReplacementNamed('/');
              }),
              buildListTile(
                  'Emergency Contacts - daerah', Icons.location_on_rounded, () {
                Navigator.of(context).pushReplacementNamed('/');
              }),
              buildListTile(
                  'Emergency Contacts - rs', Icons.location_on_rounded, () {
                Navigator.of(context)
                    .pushReplacementNamed(RumahSakitScreen.routeName);
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => RumahSakitScreen()));
              }),
            ],
          ),
        ));
  }
}
