import 'package:flutter/material.dart';

import './model/daerah.dart';
import './model/rumahsakit.dart';

const DUMMY_DAERAH = <Daerah>[
  Daerah(
    id: 'c1',
    daerah: 'Cikarang',
  ),
  Daerah(
    id: 'c2',
    daerah: 'Cikarang Baru',
  ),
  Daerah(
    id: 'c3',
    daerah: 'Cikarang Lama',
  ),
];

const DUMMY_RS = <RumahSakit>[
  RumahSakit(
    id: 'c1',
    rs: 'Rumah Sakit A',
    alamat: 'Jalan S',
    telepon: 012,
  ),
  RumahSakit(
    id: 'c2',
    rs: 'Rumah Sakit B',
    alamat: 'Jalan A',
    telepon: 021,
  ),
];
