import 'package:flutter/material.dart';
import '../main_drawer.dart';

class RumahSakitScreen extends StatelessWidget {
  static const routeName = '/rumah-sakit';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 10.0, left: 10.0),
          child: SizedBox(
            width: 700,
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      'Rumah Sakit Harapan Indah',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text('Jalan Sana Sini'),
                  ),
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      '081376150048',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Call'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: const Color(0xFF74cfbf),
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Edit'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.grey[600],
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.red, primary: Colors.white),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10.0, left: 10.0),
          child: SizedBox(
            width: 700,
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      'RS Cikarang',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text('Jalan Bebek'),
                  ),
                  const ListTile(
                    visualDensity: VisualDensity(vertical: -4),
                    title: Text(
                      '0218937722',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Text('Call'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: const Color(0xFF74cfbf),
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Edit'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.grey[600],
                            primary: Colors.white),
                      ),
                      const SizedBox(width: 3),
                      TextButton(
                        child: const Text('Delete'),
                        onPressed: () {/* ... */},
                        style: TextButton.styleFrom(
                            backgroundColor: Colors.red, primary: Colors.white),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
