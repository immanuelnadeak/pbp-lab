import 'package:flutter/material.dart';

import '../dummy_data.dart';
// import '../widgets/category_item.dart';
import '../main_drawer.dart';
import '../screen/rs_screen.dart';

class DaerahScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_DAERAH
          .map(
            (catData) => DaerahItem(
              catData.id,
              catData.daerah,
            ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}

class DaerahItem extends StatelessWidget {
  final String id;
  final String daerah;

  DaerahItem(this.id, this.daerah);

  void selectDaerah(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      RumahSakitScreen.routeName,
      arguments: {
        'id': id,
        'daerah': daerah,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectDaerah(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          daerah,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.pink.withOpacity(0.7),
              Colors.pink,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
