1. Apakah perbedaan antara JSON dan XML?
    - JSON merupakan sebuah format data, sedangkan XML adalah bahasa markup.
    - JSON adalah bagian dari sintaksis JavaScript dan sepenuhnya bebas bahasa, sedangkan XML ditulis dengan cara yang sama seperti yang diikuti oleh HTML.<
    - JSON _support_ array, sedangkan XML tidak.
    - JSON _support_ tipe data teks dan angka, termasuk integer dan string yang dapat direpresentasikan menggunakan array dan objek. Di lain sisi, XML tidak _support_ secara langsung untuk tipe array tetapi support banyak tipe data seperti angka, teks, gambar, grafik, dll.
    - JSON memiliki format data yang menggambarkan diri sendiri dan secara komparatif jauh lebih mudah dibaca daripada dokumen berformat XML. 

2. Apakah perbedaan antara HTML dan XML?
    - HTML adalah bahasa komputer yang didesain untuk menampilkan data, sedangkan XML merupakan bahasa dinamis yang didesain untuk mengirim dan mentransport data.
    - HTML berfokus pada "_how data looks_", sedangkan XML berfokus pada "_what data is_".
    - HTML memiliki kumpulan tag yang tetap/fixed, sedangkan XML memiliki kumpulan tag yang bisa diperluas.
    - Tag-tag dalam HTML mendeskripsikan cara suatu data ditampilkan, sedangkan tag-tag dalam XML mendeskripsikan konten dari data.
