from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all().values()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_note(request):
    form = NoteForm(request.POST or None)

    if (form.is_valid and request.method == 'POST'):
        # save the form data to model
        form.save()
        # If the request method is POST then we need to redirect to /lab-3
        return redirect('/lab-4')

    response = {'form': form}
    return render(request, 'lab4_form.html', response)

@login_required(login_url='/admin/login/')
def note_list(request):
    note = Note.objects.all().values()  # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)